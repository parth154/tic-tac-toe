let startBtn         =   document.querySelector('.start-btn');
let resetBtn         =   document.querySelector('.reset-btn');
let multiplayerBtn   =   document.querySelector('.multiplayer-btn');
let singleplayerBtn  =   document.querySelector('.singleplayer-btn');
let xBtn             =   document.querySelector('.x-btn');
let oBtn             =   document.querySelector('.o-btn');
let box1             =   document.body.querySelector('#box1');
let box2             =   document.body.querySelector('#box2');
let box3             =   document.body.querySelector('#box3');
let box4             =   document.body.querySelector('#box4');
let box5             =   document.body.querySelector('#box5');
let box6             =   document.body.querySelector('#box6');
let box7             =   document.body.querySelector('#box7');
let box8             =   document.body.querySelector('#box8');
let box9             =   document.body.querySelector('#box9');

let element = '';

loadEvents();
box1.style.display = 'none';
box2.style.display = 'none';
box3.style.display = 'none';
box4.style.display = 'none';
box5.style.display = 'none';
box6.style.display = 'none';
box7.style.display = 'none';
box8.style.display = 'none';
box9.style.display = 'none';
xBtn.style.display = 'none';
oBtn.style.display = 'none';
multiplayerBtn.style.display = 'none';
singleplayerBtn.style.display = 'none';
resetBtn.style.display = 'none';
document.querySelector('.start').style.display = 'block';

function loadEvents() {
    resetBtn.addEventListener('click',resetGame);
    startBtn.addEventListener('click',startGame);
    multiplayerBtn.addEventListener('click',multiplayer);
    singleplayerBtn.addEventListener('click',singleplayer);
    xBtn.addEventListener('click',xBtnClicked);
    oBtn.addEventListener('click',oBtnClicked);
}

function resetGame(){
    multiplayerBtn.style.display = 'inline-block';
    singleplayerBtn.style.display = 'inline-block'; 
    document.body.querySelector('#box1').style.display = 'none';
    document.body.querySelector('#box2').style.display = 'none';
    document.body.querySelector('#box3').style.display = 'none';
    document.body.querySelector('#box4').style.display = 'none';
    document.body.querySelector('#box5').style.display = 'none';
    document.body.querySelector('#box6').style.display = 'none';
    document.body.querySelector('#box7').style.display = 'none';
    document.body.querySelector('#box8').style.display = 'none';
    document.body.querySelector('#box9').style.display = 'none';
    xBtn.style.display = 'none';
    oBtn.style.display = 'none';
    resetBtn.style.display = 'none';
    box1.textContent = '';
    box2.textContent = '';
    box3.textContent = '';
    box4.textContent = '';
    box5.textContent = '';
    box6.textContent = '';
    box7.textContent = '';
    box8.textContent = '';
    box9.textContent = '';
}

function startGame() {
    document.body.querySelector('.start').style.display = 'none';
    multiplayerBtn.style.display = 'inline-block';
    singleplayerBtn.style.display = 'inline-block'; 
}

function multiplayer() {
    multiplayerBtn.style.display = 'none';
    singleplayerBtn.style.display = 'none';
    document.body.querySelector('#box1').style.display = 'inline-block';
    document.body.querySelector('#box2').style.display = 'inline-block';
    document.body.querySelector('#box3').style.display = 'inline-block';
    document.body.querySelector('#box4').style.display = 'inline-block';
    document.body.querySelector('#box5').style.display = 'inline-block';
    document.body.querySelector('#box6').style.display = 'inline-block';
    document.body.querySelector('#box7').style.display = 'inline-block';
    document.body.querySelector('#box8').style.display = 'inline-block';
    document.body.querySelector('#box9').style.display = 'inline-block';
    xBtn.style.display = 'inline-block';
    oBtn.style.display = 'inline-block';
    resetBtn.style.display = 'inline-block';
    box1.textContent = '';
    box2.textContent = '';
    box3.textContent = '';
    box4.textContent = '';
    box5.textContent = '';
    box6.textContent = '';
    box7.textContent = '';
    box8.textContent = '';
    box9.textContent = '';
    box1.addEventListener('click',addInBox);
    box2.addEventListener('click',addInBox);
    box3.addEventListener('click',addInBox);
    box4.addEventListener('click',addInBox);
    box5.addEventListener('click',addInBox);
    box6.addEventListener('click',addInBox);
    box7.addEventListener('click',addInBox);
    box8.addEventListener('click',addInBox);
    box9.addEventListener('click',addInBox);
}

function singleplayer() {
    multiplayerBtn.style.display = 'none';
    singleplayerBtn.style.display = 'none';
    document.body.querySelector('#box1').style.display = 'inline-block';
    document.body.querySelector('#box2').style.display = 'inline-block';
    document.body.querySelector('#box3').style.display = 'inline-block';
    document.body.querySelector('#box4').style.display = 'inline-block';
    document.body.querySelector('#box5').style.display = 'inline-block';
    document.body.querySelector('#box6').style.display = 'inline-block';
    document.body.querySelector('#box7').style.display = 'inline-block';
    document.body.querySelector('#box8').style.display = 'inline-block';
    document.body.querySelector('#box9').style.display = 'inline-block';
    xBtn.style.display = 'none';
    oBtn.style.display = 'none';
    box1.textContent = '';
    box2.textContent = '';
    box3.textContent = '';
    box4.textContent = '';
    box5.textContent = '';
    box6.textContent = '';
    box7.textContent = '';
    box8.textContent = '';
    box9.textContent = '';
    resetBtn.style.display = 'inline-block';
    box1.addEventListener('click',computer);
    box2.addEventListener('click',computer);
    box3.addEventListener('click',computer);
    box4.addEventListener('click',computer);
    box5.addEventListener('click',computer);
    box6.addEventListener('click',computer);
    box7.addEventListener('click',computer);
    box8.addEventListener('click',computer);
    box9.addEventListener('click',computer);
}

function xBtnClicked() {
    element = 'X';
}

function oBtnClicked() {
    element = 'O';
}

function setText(element,text){
    document.body.querySelector("#" + element).textContent = text;
    document.body.querySelector("#" + element).style.fontSize = '50px';
    document.body.querySelector("#" + element).style.padding = '50px';
}

function addInBox(e) {
    console.log('add');
    if(e.target.textContent != 'X' && e.target.textContent != 'O'){
        if(element!=''){
            setText(e.target.id,element);
        }
        checkWinner();
    }
    if(box1.textContent != '' && box2.textContent != '' && box3.textContent != '' && box4.textContent != '' && box5.textContent != '' && box6.textContent != '' && box7.textContent != '' && box8.textContent != '' && box9.textContent != ''){
        alert('draw')   
    }
    element = '';
}

function computer(e) {
    console.log('comp');
    element = 'X';
    if(e.target.textContent != 'X' && e.target.textContent != 'O'){
        if(e.target.textContent != 'X' && e.target.textContent != 'O'){
            if(element == 'X'){
                setText(e.target.id,element);
                checkWinner();
            } 
        }
        let random;
        let flag = 1;
        while(flag == 1){
            random = Math.round(1 + Math.random() * 8);
            if(document.body.querySelector("#box" + random).textContent == ''){
                setText("box" + random,'O');
                element = 'O';
                checkWinner();
                break;
            } else if(box1.textContent != '' && box2.textContent != '' && box3.textContent != '' && box4.textContent != '' && box5.textContent != ''        && box6.textContent != '' && box7.textContent != '' && box8.textContent != '' && box9.textContent != ''){
                flag = 0;
                alert('draw');
                resetGame();
            }
        }
    }
    if(box1.textContent != '' && box2.textContent != '' && box3.textContent != '' && box4.textContent != '' && box5.textContent != ''        && box6.textContent != '' && box7.textContent != '' && box8.textContent != '' && box9.textContent != ''){
        alert('draw')   
    }
}

function checkWinner(){
    if(box1.textContent != '' && box2.textContent != '' && box3.textContent != ''){
        if(box1.textContent == box2.textContent && box2.textContent == box3.textContent){
            if(element == 'X'){
                alert('X WON');
            } else if(element == 'O'){
                alert('O WON');
            }
            resetGame();
        }
    }
    if(box4.textContent != '' && box5.textContent != '' && box6.textContent != ''){
        if(box4.textContent == box5.textContent && box5.textContent == box6.textContent){
            if(element == 'X'){
                alert('X WON');
            } else if(element == 'O'){
                alert('O WON');
            }
            resetGame();
        }
    }
    if(box7.textContent != '' && box8.textContent != '' && box9.textContent != ''){
        if(box7.textContent == box8.textContent && box8.textContent == box9.textContent){
            if(element == 'X'){
                alert('X WON');
            } else if(element == 'O'){
                alert('O WON');
            }
            resetGame();
        }
    }
    if(box1.textContent != '' && box4.textContent != '' && box7.textContent != ''){
        if(box1.textContent == box4.textContent && box4.textContent == box7.textContent){
            if(element == 'X'){
                alert('X WON');
            } else if(element == 'O'){
                alert('O WON');
            }
            resetGame();
        }
    }
    if(box2.textContent != '' && box5.textContent != '' && box8.textContent != ''){
        if(box2.textContent == box5.textContent && box5.textContent == box8.textContent){
            if(element == 'X'){
                alert('X WON');
            } else if(element == 'O'){
                alert('O WON');
            }
            resetGame();
        }
    }
    if(box3.textContent != '' && box6.textContent != '' && box9.textContent != ''){
        if(box3.textContent == box6.textContent && box6.textContent == box9.textContent){
            if(element == 'X'){
                alert('X WON');
            } else if(element == 'O'){
                alert('O WON');
            }
            resetGame();
        }
    }
    if(box1.textContent != '' && box5.textContent != '' && box9.textContent != ''){
        if(box1.textContent == box5.textContent && box5.textContent == box9.textContent){
            if(element == 'X'){
                alert('X WON');
            } else if(element == 'O'){
                alert('O WON');
            }
            resetGame();
        }
    }
    if(box3.textContent != '' && box5.textContent != '' && box7.textContent != ''){
        if(box3.textContent == box5.textContent && box5.textContent == box7.textContent){
            if(element == 'X'){
                alert('X WON');
            } else if(element == 'O'){
                alert('O WON');
            }
            resetGame();
        }
    }
}